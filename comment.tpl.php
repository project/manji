<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
  <div class="clearer">&nbsp;</div> 
  <div class="commentname"> <span class="commentauthor"> <?php print $author ?> </span> </div> 
  <div class="commentinfo"> <span class="commentdate"> <?php print $date ?> </span> </div> 
  <div class="clearer">&nbsp;</div> 
  <div class="commenttext"> <?php print $content ?> </div> 
  <div class="commentinfo"><?php print $links ?> </div> 
</div>
