 <div class="post<?php print ($sticky) ? " sticky" : ""; ?>"> 
  <?php if ($page == 0): ?> 
  <div class="title"> <a href="<?php print $node_url ?>" rel="bookmark"> <?php print $title ?> </a> </div> 
  <?php endif; ?>
  <h3><span class="posted"><?php print $name ?> @ <?php print $date ?></span></h3> 
  <div class="storycontent"> <?php print $content ?> </div> 
  <div class="meta"> 
    <div class="feedback"> <?php print $links ?> </div> 
    <?php if ($terms != ""): ?> 
    Filed under: <?php print $terms ?> 
    <?php endif; ?> 
  </div> 
  
</div>
