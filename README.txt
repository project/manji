
Manji theme for phptemplate on Drupal
----------------------------------------

This is a port of Manji theme by Khaled Abou Alfa (http://www.brokenkode.com/manji/)

Manji is a minimalist single column theme and very customizable.  It uses valid XHTML and CSS
and should looks consistent across modern browsers.

Recommended usage:
- turn on primary links
- add "admin" (path is "admin") to the primary link
- turn off all blocks except 'navigation'
- set block path for 'navigation' to "(admin)"  (without quotes)

Page elements supported: (all can be switch on/off)
- site name
- primary links
- search box

Page elements supported: (these will not be rendered whether selected or not)
- site logo
- site slogan 
- secondary links
- pictures in comment and post



Author: Chrisada Sookdhis
Contact: use contact form at http://ichris.ws/contact