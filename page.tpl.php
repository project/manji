<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>">
<head profile="http://gmpg.org/xfn/11">
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?><?php print $styles ?>
</head>
<body <?php print theme("onload_attribute"); ?>>
<div id="container2"> 
<a name="top"></a> 
<div id="hnav"> 
  <div id="hmenu"> 
    <?php if (is_array($primary_links)) : ?> 
    <ul id="top"> 
      <?php foreach ($primary_links as $link): ?> 
      <li><?php print $link?></li> 
      <?php endforeach; ?> 
    </ul> 
    <?php endif; ?> 
  </div> 
  <div id="header"> 
    <?php if ($site_name) : ?> 
    <h1 id="blogtitle"> <a href="<?php print url() ?>"><?php print($site_name) ?> </a> </h1> 
    <?php endif; ?> 
  </div> 
</div> 
<!-- Closes the hnav div--> 
<div id="masthead" onclick="location.href='<?php print url() ?>';" style="cursor: pointer;"> </div> 
<div id="container"> 
  <div id="topcontent"></div> 
  <div id="blocks"> 
    <?php if ($sidebar_left != ""): ?> 
    <?php print $sidebar_left ?> 
    <?php endif; ?> 
    <?php if ($sidebar_right != ""): ?> 
    <?php print $sidebar_right ?> 
    <?php endif; ?> 
  </div> 
  <div id="content"> 
    <?php if ($breadcrumb != "<div class=\"breadcrumb\"></div>"): ?> 
    <div class="postnavigation"> 
      <div class="left"> <?php print $breadcrumb ?> </div> 
    </div> 
    <?php endif; ?> 
    <?php if ($tabs != ""): ?> 
    <?php print $tabs ?> 
    <?php endif; ?> 
    <?php if ($title != ""): ?> 
    <h2 class="content-title"><?php print $title ?></h2> 
    <?php endif; ?> 
    <?php if ($mission != ""): ?> 
    <div id="mission"><?php print $mission ?></div> 
    <?php endif; ?> 
    <?php if ($help != ""): ?> 
    <p id="help"><?php print $help ?></p> 
    <?php endif; ?> 
    <?php if ($messages != ""): ?> 
    <div id="message"><?php print $messages ?></div> 
    <?php endif; ?> 
    <!-- start main content --> 
    <?php print($content) ?> 
    <!-- end main content --> 
  </div> 
  <div id="bottomcontent"> </div> 
  <!-- Closes the content div--> 
</div> 
<!-- Closes the container div--> 
<div id="menu"> 
  <?php if ($search_box): ?> 
  <form id="searchform" method="post" action="<?php echo $search_url; ?>"> 
    <input id="searchbutton" type="submit" name="submit" value="<?php print $search_button_text ?>" /> 
    <input type="text" name="edit[keys]" id="search" size="15" /> 
  </form> 
  <?php endif; ?> 
  <div id="topimage"> <a href="#top"></a> </div> 
</div> 
<div id="footer"> 
  <p class="credits"> 
    <?php if ($footer_message) : ?> 
    <?php print $footer_message;?> 
    <?php endif; ?> 
    <a href="<?php print url() ?>"><?php print($site_name) ?></a> is powered by <a href="http://drupal.org">Drupal</a>. <a href="http://www.brokenkode.com/manji">Manji Theme</a> by Khaled Abou Alfa and Root. Obsidian &#169; 2004 <a href="http://scottdot.org/">ScottDot</a>. Syndicate this site using <a href="<?php print url() ?>/node/feed" title="Syndicate this site using RSS"> <abbr title="Really Simple Syndication">RSS</abbr></a>.<br /> 
    <a href="http://validator.w3.org/check/referer">xhtml 1.0 trans </a>/ <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS </a>.
	</p>  
</div> 
<a name = "bottom"></a>
</div> 
<?php print $closure;?> 
</body>
</html>
